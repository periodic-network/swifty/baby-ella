  //////////////////////////////////////////////////////////////
 // Modules                                                  //
//////////////////////////////////////////////////////////////
var discord = require('discord.js');
require('dotenv').config();

  //////////////////////////////////////////////////////////////
 // Variables                                                //
//////////////////////////////////////////////////////////////
const presence_list = [
  "with BabyOri 💕",
  "with BabyAnika 💕",
  "with Discord 💕",
  "on My phone 💕",
  "Fighting BabyOri 😡",
  "Survival on Periodic 💕",
  "on periodic.play.ai",
  "Taylor Swift on Loop...",
  "Minecraft 💕",
  "configuring perms",
  "annoying Peri",
  "on periodic.play.ai",
  "on fallenuniverse.net",
  "with Swifty's code",
];

const message_list = [
  "I don't know you....",
  "Are you a man or a woman?",
  "Sorry, Baby Ella cannot answer the call right now, Why? Because im trying to sleep! <:pan_rage:591672938130964523>",
  "What?",
  "How many people does it take to make a singular earth?",
  "Don't hit me! :sob:",
  "Shut up and let me drink my hot chocolate <:anniecoffee:591672906174431278>",
  "<:thonking:591672945534042115>",
  ":runner:"
];

  //////////////////////////////////////////////////////////////
 // Declare the User                                         //
//////////////////////////////////////////////////////////////
var client = new discord.Client();

  //////////////////////////////////////////////////////////////
 // On Message                                               //
//////////////////////////////////////////////////////////////
client.on('message', async (message) => {
	if(message.author.bot) return;

  if(message.channel.type == `dm`) {
    if(message.author.id !== "568919303580024862" && message.author.id !== "565299565260046368" && message.author.id !== "200692983471931392") return;
    let msg = message.content
    if(msg) {
      let embed = new discord.RichEmbed()
      embed.setTitle(`Announcement by ${message.author.username}`)
      embed.setAuthor('Baby Ella Bot')
      embed.setDescription(msg)
      embed.setColor(0xF08080)

      let sChannel = message.client.channels.get("591780561287249926")
      sChannel.send('please work');
      sChannel.send(embed);
      console.log(`An announcement has been made by ${message.author.tag}\n ${msg}`)
    } else {
      return;
    }
  }

  if (message.mentions.users.first() && message.mentions.users.first().id === client.user.id) {
    let mentionmanager = require('./managers/mentionmanager.js');
    mentionmanager.run(client, message, message_list);
    return;
  }

  var prefix = process.env.PREFIX;
	if(message.content.startsWith(prefix)) {
		var command = message.content.split(' ')[0].slice(prefix.length);
		try {
			let commands = require('./commands/'+command.toLowerCase());
			commands.run(client, message);
		} catch(e) {
			console.log(e);
		}
	}
});

  //////////////////////////////////////////////////////////////
 // On Ready                                                 //
//////////////////////////////////////////////////////////////
client.on('ready', () => {
  console.log('Bot Online!');
  setInterval(() => {
      const index = Math.floor(Math.random() * (presence_list.length - 1) + 1);
      client.user.setActivity(presence_list[index]);
  }, 20000);
});

  //////////////////////////////////////////////////////////////
 // Login                                                    //
//////////////////////////////////////////////////////////////
client.login(process.env.TOKEN);
