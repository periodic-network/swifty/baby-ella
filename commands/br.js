var discord = require('discord.js');

module.exports.run = async (client, message) => {
	var prefix = process.env.PREFIX;
  var suggestion = message.content.split(' ').slice(1).join(' ');
  if(suggestion) {
    const embed = new discord.RichEmbed()
    embed.setAuthor('New Bug Report')
    embed.setTitle('BUG REPORT INCOMING')
    .setColor(0xF08080)
    embed.setThumbnail(`${message.author.displayAvatarURL}`)
    embed.addField('**Bug Report:**', `${suggestion}`)
    embed.addField('Bug Report By:', `${message.author.tag}`)
    message.channel.send(embed)
    message.delete();
  } else {
    message.reply(`please try ${prefix}br <bug report>`);
  }
};
