var discord = require('discord.js');

module.exports.run = async (client, message) => {
	var prefix = process.env.PREFIX;
  var suggestion = message.content.split(' ').slice(1).join(' ');
  if(suggestion) {
    const embed = new discord.RichEmbed()
    embed.setAuthor('New Suggestion')
    embed.setTitle('Upvote | Downvote')
    embed.setColor(0x008080)
    embed.setThumbnail(`${message.author.displayAvatarURL}`)
    embed.addField('**Suggestion:**', `${suggestion}`)
    embed.addField('Suggestion by:', `${message.author.tag}`)
    message.channel.send(embed).then(async suggestion => {
      await suggestion.react('👍');
      await suggestion.react('👎');
    });
    message.delete();
  } else {
    message.reply(`please try ${prefix}su <suggestion>`);
  }
};
