var discord = require('discord.js');

module.exports.run = async (client, message, message_list) => {
	var prefix = process.env.PREFIX
	var cmd = message.content.split(' ').slice(1).join(' ').toLowerCase();
	switch(cmd) {
		case "friends?":
			message.channel.send('Sure!');
		  break;
		case "how are you?":
			message.reply('I am good!');
		  break;
		case "who do you love":
			message.reply('My Family! <:pan_love:591672929792557056>');
		  break;
		case "what is the meaning of":
	    message.reply('How the fuck am I supposed to know?');
		  break;
		case "are we good?":
	    message.reply('Yeah, I just felt a bit sick :nauseated_face:');
		  break;
		case "wheel":
	    message.channel.send(':cartwheel:');
		  break;
		case "love me":
	    message.reply(`I love you :heart: <:pan_love:591672929792557056>`);
		  break;
		case "hub":
	    message.channel.send('<:briheart:591672912629465120>')
		  break;
		case "favorite color?":
        const embed = new discord.RichEmbed();
        embed.setAuthor('Baby Rio\'s Favorite Color :)');
        embed.setDescription('Light Coral (Red)\nHex Code: #F08080');
        embed.setColor(0xF08080);
        message.channel.send({embed});
		  break;
		case "survival":
        const embed2 = new discord.RichEmbed();
        embed2.setAuthor('- New Survival Updates -');
        embed2.setTitle('+ Added | - Removed | / Working on');
        embed2.setColor(0x008080);
        embed2.setDescription('**Description**');

        message.channel.send(embed2);
		  break;
		default:
		  const index = Math.floor(Math.random() * (message_list.length - 1) + 1);
		  message.channel.send(message_list[index]);
		  break;
	}
};
